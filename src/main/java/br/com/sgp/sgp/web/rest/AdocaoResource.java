package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Adocao;
import br.com.sgp.sgp.model.Animal;
import br.com.sgp.sgp.service.AdocaoService;
import br.com.sgp.sgp.service.AnimalService;
import br.com.sgp.sgp.service.dto.AdocaoDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/adocao")
public class AdocaoResource {

    private static final String ENTITY_NAME = "Adocao";
    private static final String BASE_URI = "/api/adocao/";

    private final AdocaoService adocaoService;
    private final AnimalService animalService;

    public AdocaoResource(final AdocaoService adocaoService, final AnimalService animalService) {
        this.adocaoService = adocaoService;
        this.animalService = animalService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<AdocaoDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(adocaoService.findAll(), AdocaoDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Adocao>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(adocaoService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Adocao>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(adocaoService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdocaoDTO> findById(@PathVariable final Long id) {
        return adocaoService.findById(id)
                .map(adocao -> ResponseEntity.ok(ObjectMapperUtils.map(adocao, AdocaoDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid AdocaoDTO adocaoDTO) throws URISyntaxException {
        final Adocao adocao = adocaoService.save(ObjectMapperUtils.map(adocaoDTO, new Adocao()));

        adocao.getAnimais().forEach(it -> {
            final Optional<Animal> animalOpt = animalService.findById(it.getId());
            if (animalOpt.isPresent()) {
                final Animal animal = animalOpt.get();
                animal.setStatus(true);
                animal.setAdocao(adocao);
                animal.setBaia(null);
                animalService.save(animal);
            }
        });

        return ResponseEntity.created(new URI(BASE_URI + adocao.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(adocao.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid AdocaoDTO adocaoDTO) {
        final Adocao adocaoDb = adocaoService.findById(adocaoDTO.getId()).get();
        final List<Animal> animaisDb = adocaoDb.getAnimais();
        animaisDb.forEach(it -> {
            it.setStatus(false);
            it.setAdocao(null);
            animalService.save(it);
        });

        final Adocao adocao = adocaoService.save(ObjectMapperUtils.map(adocaoDTO, new Adocao()));

        adocao.getAnimais().forEach(it -> {
            final Optional<Animal> animalOpt = animalService.findById(it.getId());
            if (animalOpt.isPresent()) {
                final Animal animal = animalOpt.get();
                animal.setStatus(true);
                animal.setAdocao(adocao);
                animalService.save(animal);
            }
        });

        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(adocao.getId())))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Adocao> adocaoOpt = adocaoService.findById(id);
        if (adocaoOpt.isPresent()) {
            try {
                final Adocao adocao = adocaoOpt.get();
                adocao.getAnimais().forEach(it -> {
                    it.setStatus(false);
                    it.setAdocao(null);
                    animalService.save(it);
                });
                adocaoService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
