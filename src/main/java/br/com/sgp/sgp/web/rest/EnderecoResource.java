package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Endereco;
import br.com.sgp.sgp.service.EnderecoService;
import br.com.sgp.sgp.service.dto.EnderecoDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/endereco")
public class EnderecoResource {

    private static final String ENTITY_NAME = "Endereco";
    private static final String BASE_URI = "/endereco/";

    private final EnderecoService enderecoService;

    public EnderecoResource(final EnderecoService enderecoService) {
        this.enderecoService = enderecoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<EnderecoDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(enderecoService.findAll(), EnderecoDTO.class)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EnderecoDTO> findById(@PathVariable final Long id) {
        return enderecoService.findById(id)
                .map(endereco -> ResponseEntity.ok(ObjectMapperUtils.map(endereco, EnderecoDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid EnderecoDTO enderecoDTO) throws URISyntaxException {
        final Endereco endereco = enderecoService.save(ObjectMapperUtils.map(enderecoDTO, new Endereco()));
        return ResponseEntity.created(new URI(BASE_URI + endereco.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(endereco.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid EnderecoDTO enderecoDTO) {
        final Endereco endereco = enderecoService.save(ObjectMapperUtils.map(enderecoDTO, new Endereco()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(endereco.getId())))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Endereco> endereco = enderecoService.findById(id);
        if (endereco.isPresent()) {
            try {
                enderecoService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
