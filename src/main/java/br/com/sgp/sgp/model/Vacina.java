package br.com.sgp.sgp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Vacina extends BasicModel {

    private String nome;
    private String marca;
    private String lote;
    private String fabricante;
    private Date validade;
    private BigDecimal intervaloDeDias;
    private BigDecimal quantidadeAplicacao;
    @ManyToOne
    @JoinColumn(name = "id_tipo_vacina")
    private TipoVacina tipoVacina;
}
