package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Adotante;

@Repository
public interface AdotanteRepository extends JpaRepository<Adotante, Long> {

    List<Adotante> findAllByCpfEqualsIgnoreCase(final String cpf);

    List<Adotante> findAllByRgEqualsIgnoreCase(final String rg);

    @Query("SELECT A FROM Adotante A WHERE A.nome LIKE %?1% OR A.rg LIKE %?1% OR A.cpf LIKE %?1% OR A.telefone LIKE %?1%")
    Page<Adotante> findBySearch(final String searchTerm, final Pageable pageable);
}
