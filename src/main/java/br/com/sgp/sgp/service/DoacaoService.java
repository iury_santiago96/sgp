package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Doacao;
import br.com.sgp.sgp.repository.DoacaoRepository;

@Service
public class DoacaoService {

    private final DoacaoRepository doacaoRepository;

    public DoacaoService(final DoacaoRepository doacaoRepository) {
        this.doacaoRepository = doacaoRepository;
    }

    public List<Doacao> findAll() {
        return doacaoRepository.findAll();
    }

    public Page<Doacao> findAllPageable(final String searchTerm, final Pageable pageable) {
        return doacaoRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Doacao> findById(final Long id) {
        return doacaoRepository.findById(id);
    }

    public Doacao save(final Doacao doacao) {
        return doacaoRepository.save(doacao);
    }

    public void deleteById(final Long id) {
        doacaoRepository.deleteById(id);
    }

    public Long countDoacoes() {
        return doacaoRepository.count();
    }
}
