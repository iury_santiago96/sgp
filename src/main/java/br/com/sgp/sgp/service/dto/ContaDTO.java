package br.com.sgp.sgp.service.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ContaDTO {

    private Long id;
    private BigDecimal valor;
    private String nome;
    private Date dtConta;
    private TipoContaDTO tipoConta;
    private VoluntarioDTO voluntario;
}
