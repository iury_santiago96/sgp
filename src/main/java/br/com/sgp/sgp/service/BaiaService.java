package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Baia;
import br.com.sgp.sgp.repository.BaiaRepository;

@Service
public class BaiaService {

    private final BaiaRepository baiaRepository;

    public BaiaService(final BaiaRepository baiaRepository) {
        this.baiaRepository = baiaRepository;
    }

    public List<Baia> findAll() {
        return baiaRepository.findAll();
    }

    public Page<Baia> findAllPageable(final String searchTerm, final Pageable pageable) {
        return baiaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Baia> findById(final Long id) {
        return baiaRepository.findById(id);
    }

    public Baia save(final Baia baia) {
        return baiaRepository.save(baia);
    }

    public void deleteById(final Long id) {
        baiaRepository.deleteById(id);
    }

    public List<Baia> findAllByNomeEqualsIgnoreCase(final String nome) {
        return baiaRepository.findAllByNomeEqualsIgnoreCase(nome);
    }
}
