package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Vacina;
import br.com.sgp.sgp.service.VacinaService;
import br.com.sgp.sgp.service.dto.VacinaDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/vacina")
public class VacinaResource {

    private static final String ENTITY_NAME = "Vacina";
    private static final String BASE_URI = "/api/vacina/";

    private final VacinaService vacinaService;

    public VacinaResource(final VacinaService vacinaService) {
        this.vacinaService = vacinaService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<VacinaDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(vacinaService.findAll(), VacinaDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Vacina>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(vacinaService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Vacina>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(vacinaService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<VacinaDTO> findById(@PathVariable final Long id) {
        return vacinaService.findById(id)
                .map(vacina -> ResponseEntity.ok(ObjectMapperUtils.map(vacina, VacinaDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid VacinaDTO vacinaDTO) throws URISyntaxException {
        final Vacina vacina = vacinaService.save(ObjectMapperUtils.map(vacinaDTO, new Vacina()));
        return ResponseEntity.created(new URI(BASE_URI + vacina.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(vacina.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid VacinaDTO vacinaDTO) {
        final Vacina vacina = vacinaService.save(ObjectMapperUtils.map(vacinaDTO, new Vacina()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(vacina.getId())))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Vacina> vacina = vacinaService.findById(id);
        if (vacina.isPresent()) {
            try {
                vacinaService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
