package br.com.sgp.sgp.service.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AnimaisBaiaDTO implements Serializable {

    private static final long serialVersionUID = -4879673672324659221L;
    private Long total;
    private String baia;
    private Integer capacidade;

    public AnimaisBaiaDTO(final Long total, final String baia, final Integer capacidade) {
        this.total = total;
        this.baia = baia;
        this.capacidade = capacidade;
    }
}
