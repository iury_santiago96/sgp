package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.TipoVoluntario;
import br.com.sgp.sgp.service.TipoVoluntarioService;
import br.com.sgp.sgp.service.dto.TipoVoluntarioDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/tipo-voluntario")
public class TipoVoluntarioResource {

    private static final String ENTITY_NAME = "Tipo de Voluntario";
    private static final String BASE_URI = "/api/tipo-voluntario/";

    private final TipoVoluntarioService tipoVoluntarioService;

    public TipoVoluntarioResource(final TipoVoluntarioService tipoVoluntarioService) {
        this.tipoVoluntarioService = tipoVoluntarioService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<TipoVoluntarioDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(tipoVoluntarioService.findAll(), TipoVoluntarioDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<TipoVoluntario>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoVoluntarioService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<TipoVoluntario>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoVoluntarioService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoVoluntarioDTO> findById(@PathVariable final Long id) {
        return tipoVoluntarioService.findById(id)
                .map(voluntario -> ResponseEntity.ok(ObjectMapperUtils.map(voluntario, TipoVoluntarioDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid TipoVoluntarioDTO tipoVoluntarioDTO) throws URISyntaxException {
        final List<TipoVoluntario> tipos = tipoVoluntarioService.findAllByTipoEqualsIgnoreCase(tipoVoluntarioDTO.getTipo());

        if (tipos.isEmpty()) {
            final TipoVoluntario
                    tipoVoluntario =
                    tipoVoluntarioService.save(ObjectMapperUtils.map(tipoVoluntarioDTO, new TipoVoluntario()));
            return ResponseEntity.created(new URI(BASE_URI + tipoVoluntario.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(tipoVoluntario.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um tipo de voluntario com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid TipoVoluntarioDTO tipoVoluntarioDTO) {
        final List<TipoVoluntario> tipos = tipoVoluntarioService.findAllByTipoEqualsIgnoreCase(tipoVoluntarioDTO.getTipo());

        if (tipos.isEmpty() || tipos.stream().allMatch(it -> it.getId().equals(tipoVoluntarioDTO.getId()))) {
            final TipoVoluntario
                    tipoVoluntario =
                    tipoVoluntarioService.save(ObjectMapperUtils.map(tipoVoluntarioDTO, new TipoVoluntario()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(tipoVoluntario.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um tipo de voluntario com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<TipoVoluntario> tipoVoluntario = tipoVoluntarioService.findById(id);
        if (tipoVoluntario.isPresent()) {
            try {
                tipoVoluntarioService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
