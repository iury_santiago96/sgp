package br.com.sgp.sgp.service.dto;

import lombok.Data;

@Data
public class UsuarioDTO {

    private Long id;
    private String login;
    private String senha;
    private VoluntarioDTO voluntario;

}
