package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Adotante;
import br.com.sgp.sgp.service.AdotanteService;
import br.com.sgp.sgp.service.dto.AdotanteDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/adotante")
public class AdotanteResource {

    private static final String ENTITY_NAME = "Adotante";
    private static final String BASE_URI = "/api/adotante/";

    private final AdotanteService adotanteService;

    public AdotanteResource(final AdotanteService adotanteService) {
        this.adotanteService = adotanteService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<AdotanteDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(adotanteService.findAll(), AdotanteDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Adotante>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(adotanteService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Adotante>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(adotanteService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdotanteDTO> findById(@PathVariable final Long id) {
        return adotanteService.findById(id)
                .map(adotante -> ResponseEntity.ok(ObjectMapperUtils.map(adotante, AdotanteDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid AdotanteDTO adotanteDTO) throws URISyntaxException {
        final List<Adotante> cpfs = adotanteService.findAllByCpfEqualsIgnoreCase(adotanteDTO.getCpf());
        final List<Adotante> rgs = adotanteService.findAllByRgEqualsIgnoreCase(adotanteDTO.getRg());

        if (cpfs.isEmpty() && rgs.isEmpty()) {
            final Adotante adotante = adotanteService.save(ObjectMapperUtils.map(adotanteDTO, new Adotante()));
            return ResponseEntity.created(new URI(BASE_URI + adotante.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(adotante.getId()))).build();
        } else {
            if (!cpfs.isEmpty() && !rgs.isEmpty()) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo rg e cpf no sistema!"))
                        .build();
            } else if (rgs.isEmpty()) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo cpf no sistema!"))
                        .build();
            } else {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo rg no sistema!"))
                        .build();
            }
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid AdotanteDTO adotanteDTO) {
        final List<Adotante> cpfs = adotanteService.findAllByCpfEqualsIgnoreCase(adotanteDTO.getCpf());
        final List<Adotante> rgs = adotanteService.findAllByRgEqualsIgnoreCase(adotanteDTO.getRg());

        if (cpfs.isEmpty() && rgs.isEmpty() || cpfs.stream().allMatch(it -> it.getId().equals(adotanteDTO.getId())) && rgs.stream()
                .allMatch(it -> it.getId().equals(adotanteDTO.getId()))) {
            final Adotante adotante = adotanteService.save(ObjectMapperUtils.map(adotanteDTO, new Adotante()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(adotante.getId())))
                    .build();
        } else {
            if (!cpfs.isEmpty() && !rgs.isEmpty() && !cpfs.stream().allMatch(it -> it.getId().equals(adotanteDTO.getId()))
                && !rgs.stream().allMatch(it -> it.getId().equals(adotanteDTO.getId()))) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo rg e cpf no sistema!"))
                        .build();
            } else if (!rgs.isEmpty() && !rgs.stream().allMatch(it -> it.getId().equals(adotanteDTO.getId()))) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo rg no sistema!"))
                        .build();
            } else {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um adotante com o mesmo cpf no sistema!"))
                        .build();
            }
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Adotante> adotante = adotanteService.findById(id);
        if (adotante.isPresent()) {
            try {
                adotanteService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
