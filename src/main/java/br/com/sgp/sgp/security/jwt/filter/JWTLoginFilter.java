package br.com.sgp.sgp.security.jwt.filter;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.sgp.sgp.security.jwt.UserCredentials;
import br.com.sgp.sgp.security.jwt.service.TokenAuthenticationService;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    public JWTLoginFilter(final String url, final AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest httpServletRequest,
                                                final HttpServletResponse httpServletResponse)
            throws AuthenticationException, IOException {
        final UserCredentials creds = new ObjectMapper().readValue(httpServletRequest.getInputStream(), UserCredentials.class);

        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        creds.getUsername(),
                        creds.getPassword(),
                        Collections.emptyList()
                )
        );
    }


    @Override
    protected void successfulAuthentication(
            final HttpServletRequest req,
            final HttpServletResponse res, final FilterChain chain,
            final Authentication auth
    ) throws IOException, ServletException {
        TokenAuthenticationService.addAuthentication(res, auth.getName());
    }
}
