package br.com.sgp.sgp.service.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AnimalDTO {

    private Long id;
    private String nome;
    private Date dtNascimento;
    private Date dtEntrada;
    private Boolean status;
    private BaiaDTO baia;
    private RacaDTO raca;
    @JsonIgnore
    private AdocaoDTO adocao;

}
