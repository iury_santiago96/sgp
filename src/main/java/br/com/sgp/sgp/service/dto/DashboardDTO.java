package br.com.sgp.sgp.service.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DashboardDTO implements Serializable {

    private static final long serialVersionUID = 7982024376563092243L;
    private Long totalAnimais;
    private Long totalAnimaisAdotados;
    private Long totalFichas;
    private Long totalDoacoes;
    private Long totalVoluntarios;
    private List<AnimaisBaiaDTO> totalAnimaisPorBaia;
    private List<AdotantesAnimaisDTO> topDezAdotantes;
    private DashboardContaDTO graficoConta;

    public DashboardDTO totalAnimais(final Long totalAnimais) {
        this.totalAnimais = totalAnimais;
        return this;
    }

    public DashboardDTO totalAnimaisAdotados(final Long totalAnimaisAdotados) {
        this.totalAnimaisAdotados = totalAnimaisAdotados;
        return this;
    }

    public DashboardDTO totalFichas(final Long totalFichas) {
        this.totalFichas = totalFichas;
        return this;
    }

    public DashboardDTO totalDoacoes(final Long totalDoacoes) {
        this.totalDoacoes = totalDoacoes;
        return this;
    }

    public DashboardDTO totalVoluntarios(final Long totalVoluntarios) {
        this.totalVoluntarios = totalVoluntarios;
        return this;
    }

    public DashboardDTO totalAnimaisPorBaia(final List<AnimaisBaiaDTO> totalAnimaisPorBaia) {
        this.totalAnimaisPorBaia = totalAnimaisPorBaia;
        return this;
    }

    public DashboardDTO topDezAdotantes(final List<AdotantesAnimaisDTO> topDezAdotantes) {
        this.topDezAdotantes = topDezAdotantes;
        return this;
    }

    public DashboardDTO graficoConta(final DashboardContaDTO graficoConta) {
        this.graficoConta = graficoConta;
        return this;
    }
}