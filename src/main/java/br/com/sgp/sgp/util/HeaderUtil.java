package br.com.sgp.sgp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

public final class HeaderUtil {

    private static final Logger LOG = LoggerFactory.getLogger(HeaderUtil.class);

    private HeaderUtil() {
    }

    public static HttpHeaders createAlert(final String message, final String param) {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("X-App-alert", message);
        headers.add("X-App-params", param);
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(final String entityName, final String param) {
        return createAlert(String.format("Um(a) novo(a) %s foi criado(a)", entityName), param);
    }

    public static HttpHeaders createEntityUpdateAlert(final String entityName, final String param) {
        return createAlert(String.format("Um(a) %s foi atualizado(a)", entityName), param);
    }

    public static HttpHeaders createEntityDeletionAlert(final String entityName, final String param) {
        return createAlert(String.format("Um(a) %s foi deletado(a)", entityName), param);
    }

    public static HttpHeaders createFailureAlert(final String entityName, final String defaultMessage) {
        LOG.error("Entity processing failed, {}", defaultMessage);
        final HttpHeaders headers = new HttpHeaders();
        headers.add("X-App-error", defaultMessage);
        headers.add("X-App-params", entityName);
        return headers;
    }

    public static HttpHeaders createFailure(final String defaultMessage) {
        LOG.error("Entity processing failed, {}", defaultMessage);
        final HttpHeaders headers = new HttpHeaders();
        headers.add("X-App-error", defaultMessage);
        return headers;
    }
}
