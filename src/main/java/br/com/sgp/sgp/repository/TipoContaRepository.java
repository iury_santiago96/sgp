package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.TipoConta;

@Repository
public interface TipoContaRepository extends JpaRepository<TipoConta, Long> {

    List<TipoConta> findAllByTipoEqualsIgnoreCase(final String tipo);

    @Query("SELECT tc FROM TipoConta tc WHERE tc.tipo LIKE %?1% OR tc.descricao LIKE %?1%")
    Page<TipoConta> findBySearch(final String searchTerm, final Pageable pageable);
}
