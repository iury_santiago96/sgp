package br.com.sgp.sgp.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ContaResultDTO implements Serializable {

    private static final long serialVersionUID = -2513909313191487088L;

    private BigDecimal total;
    private String tipo;
    private Integer mes;

    public ContaResultDTO(final BigDecimal total, final String tipo, final Integer mes) {
        this.total = total;
        this.tipo = tipo;
        this.mes = mes;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }
}
