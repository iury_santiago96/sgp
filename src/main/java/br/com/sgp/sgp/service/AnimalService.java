package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Animal;
import br.com.sgp.sgp.repository.AnimalRepository;
import br.com.sgp.sgp.service.dto.AnimaisBaiaDTO;

@Service
public class AnimalService {

    private final AnimalRepository animalRepository;

    public AnimalService(final AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }
    public List<Animal> findAll() {
        return animalRepository.findAll();
    }

    public Page<Animal> findAllPageable(final String searchTerm, final Pageable pageable) {
        return animalRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Animal> findById(final Long id) {
        return animalRepository.findById(id);
    }

    public Animal save(final Animal animal) {
        return animalRepository.save(animal);
    }

    public void deleteById(final Long id) {
        animalRepository.deleteById(id);
    }

    public Long countAnimais() {
        return animalRepository.count();
    }

    public Long countAnimalsByStatus() {
        return animalRepository.countAnimalsByStatus(true);
    }

    public List<AnimaisBaiaDTO> findAllGroupedBaia() {
        return animalRepository.findAllGroupedBaia();
    }

    public List<Animal> findByStatusEqualsOrStatusNull() {
        return animalRepository.findByStatusEqualsOrStatusNull(false);
    }
}
