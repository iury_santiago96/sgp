package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Raca;
import br.com.sgp.sgp.service.RacaService;
import br.com.sgp.sgp.service.dto.RacaDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/raca")
public class RacaResource {

    private static final String ENTITY_NAME = "Raca";
    private static final String BASE_URI = "/api/raca/";

    private final RacaService racaService;

    public RacaResource(final RacaService racaService) {
        this.racaService = racaService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<RacaDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(racaService.findAll(), RacaDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Raca>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(racaService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Raca>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(racaService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<RacaDTO> findById(@PathVariable final Long id) {
        return racaService.findById(id)
                .map(raca -> ResponseEntity.ok(ObjectMapperUtils.map(raca, RacaDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid RacaDTO racaDTO) throws URISyntaxException {
        final List<Raca> racas = racaService.findAllByNomeEqualsIgnoreCase(racaDTO.getNome());

        if (racas.isEmpty() || racas.stream().allMatch(it -> it.getId().equals(racaDTO.getId()))) {
            final Raca raca = racaService.save(ObjectMapperUtils.map(racaDTO, new Raca()));
            return ResponseEntity.created(new URI(BASE_URI + raca.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(raca.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe uma raça com o mesmo nome no sistema!")).build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid RacaDTO racaDTO) {
        final List<Raca> racas = racaService.findAllByNomeEqualsIgnoreCase(racaDTO.getNome());

        if (racas.isEmpty()) {
            final Raca raca = racaService.save(ObjectMapperUtils.map(racaDTO, new Raca()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(raca.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe uma raça com o mesmo nome no sistema!")).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Raca> raca = racaService.findById(id);
        if (raca.isPresent()) {
            try {
                racaService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Não encontrado")).build();
        }
    }
}
