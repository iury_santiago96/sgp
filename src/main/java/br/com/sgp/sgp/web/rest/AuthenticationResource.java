package br.com.sgp.sgp.web.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.security.SecurityUtils;
import br.com.sgp.sgp.security.jwt.TokenProvider;
import br.com.sgp.sgp.service.UsuarioService;
import br.com.sgp.sgp.service.dto.LoginDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

@RestController
public class AuthenticationResource {

    @Autowired
    private final TokenProvider tokenProvider;
    @Autowired
    private final AuthenticationManager customAuthenticationManager;
    @Autowired
    private final UsuarioService usuarioService;

    public AuthenticationResource(final UsuarioService usuarioService,
                                  final TokenProvider tokenProvider,
                                  @Qualifier("customAuthenticationManager") final AuthenticationManager customAuthenticationManager) {
        this.usuarioService = usuarioService;
        this.tokenProvider = tokenProvider;
        this.customAuthenticationManager = customAuthenticationManager;
    }

    @GetMapping("/current-user")
    public ResponseEntity<?> getSession(){
        return ResponseEntity.ok(SecurityUtils.getCurrentUserLogin().map(usuarioService::findUsuarioByLogin));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@RequestBody final @Valid LoginDTO loginDTO) {

        final UsernamePasswordAuthenticationToken
                authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword());

        final Authentication authentication = customAuthenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = tokenProvider.createToken(authentication, false);
        final HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token);

        return new ResponseEntity<>(new JWTToken(token), headers, HttpStatus.OK);
    }


    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(final String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(final String idToken) {
            this.idToken = idToken;
        }
    }

}

