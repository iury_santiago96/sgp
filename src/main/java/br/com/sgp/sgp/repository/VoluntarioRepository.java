package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Voluntario;

@Repository
public interface VoluntarioRepository extends JpaRepository<Voluntario, Long> {

    List<Voluntario> findAllByCpfEqualsIgnoreCase(final String cpf);

    List<Voluntario> findAllByRgEqualsIgnoreCase(final String rg);

    @Query("SELECT v FROM Voluntario v WHERE v.nome LIKE %?1% OR v.rg LIKE %?1% OR v.cpf LIKE %?1% OR v.telefone LIKE %?1% OR v.tipoVoluntario.tipo LIKE %?1%")
    Page<Voluntario> findBySearch(final String searchTerm, final Pageable pageable);
}
