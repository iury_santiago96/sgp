package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Procedimento;
import br.com.sgp.sgp.service.ProcedimentoService;
import br.com.sgp.sgp.service.dto.ProcedimentoDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/procedimento")
public class ProcedimentoResource {

    private static final String ENTITY_NAME = "Procedimento";
    private static final String BASE_URI = "/api/procedimento/";

    private final ProcedimentoService procedimentoService;

    public ProcedimentoResource(final ProcedimentoService procedimentoService) {
        this.procedimentoService = procedimentoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ProcedimentoDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(procedimentoService.findAll(), ProcedimentoDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Procedimento>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(procedimentoService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Procedimento>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(procedimentoService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProcedimentoDTO> findById(@PathVariable final Long id) {
        return procedimentoService.findById(id)
                .map(procedimento -> ResponseEntity.ok(ObjectMapperUtils.map(procedimento, ProcedimentoDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid ProcedimentoDTO procedimentoDTO) throws URISyntaxException {
        final List<Procedimento> tipos = procedimentoService.findAllByTipoEqualsIgnoreCase(procedimentoDTO.getTipo());

        if (tipos.isEmpty()) {
            final Procedimento procedimento = procedimentoService.save(ObjectMapperUtils.map(procedimentoDTO, new Procedimento()));
            return ResponseEntity.created(new URI(BASE_URI + procedimento.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(procedimento.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um procedimento com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid ProcedimentoDTO procedimentoDTO) {
        final List<Procedimento> tipos = procedimentoService.findAllByTipoEqualsIgnoreCase(procedimentoDTO.getTipo());

        if (tipos.isEmpty() || tipos.stream().allMatch(it -> it.getId().equals(procedimentoDTO.getId()))) {
            final Procedimento procedimento = procedimentoService.save(ObjectMapperUtils.map(procedimentoDTO, new Procedimento()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(procedimento.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um procedimento com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Procedimento> procedimento = procedimentoService.findById(id);
        if (procedimento.isPresent()) {
            try {
                procedimentoService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
