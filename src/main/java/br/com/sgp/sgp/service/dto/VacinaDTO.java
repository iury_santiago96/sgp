package br.com.sgp.sgp.service.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VacinaDTO {

    private Long id;
    private String nome;
    private String marca;
    private String lote;
    private String fabricante;
    private BigDecimal intervaloDeDias;
    private BigDecimal quantidadeAplicacao;
    private Date validade;
    private TipoVacinaDTO tipoVacina;
}
