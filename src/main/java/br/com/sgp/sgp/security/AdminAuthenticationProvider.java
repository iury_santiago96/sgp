package br.com.sgp.sgp.security;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import br.com.sgp.sgp.model.Usuario;
import br.com.sgp.sgp.repository.UsuarioRepository;

@Component
public class AdminAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final Usuario usuario = usuarioRepository.findOneByLogin(authentication.getName()).orElse(null);

        final Authentication
                auth =
                new UsernamePasswordAuthenticationToken(usuario.getLogin(), usuario.getSenha(), new LinkedList<>());

        return null;
    }

    @Override
    public boolean supports(final Class<?> aClass) {
        return false;
    }
}
