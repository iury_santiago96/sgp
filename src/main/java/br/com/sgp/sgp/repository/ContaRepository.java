package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Conta;
import br.com.sgp.sgp.service.dto.ContaResultDTO;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Long> {

    @Query("SELECT C FROM Conta C WHERE C.nome LIKE %?1% OR C.tipoConta.tipo LIKE %?1% OR C.voluntario.nome LIKE %?1%")
    Page<Conta> findBySearch(final String searchTerm, final Pageable pageable);

    @Query("SELECT new br.com.sgp.sgp.service.dto.ContaResultDTO(sum(c.valor), t.tipo, EXTRACT(MONTH FROM c.dtConta)) "
           + "FROM Conta AS c INNER JOIN c.tipoConta as t "
           + "WHERE CAST(EXTRACT(YEAR FROM c.dtConta) as text) = ?1 "
           + "GROUP BY t.tipo, EXTRACT(MONTH FROM c.dtConta) "
           + "ORDER BY EXTRACT(MONTH FROM c.dtConta)")
    List<ContaResultDTO> findContasByYear(final String ano);
}
