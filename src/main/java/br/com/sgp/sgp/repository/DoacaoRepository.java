package br.com.sgp.sgp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Doacao;

@Repository
public interface DoacaoRepository extends JpaRepository<Doacao, Long> {

    @Query("SELECT D FROM Doacao D WHERE D.descricao LIKE %?1% OR D.voluntario.nome LIKE %?1%")
    Page<Doacao> findBySearch(final String searchTerm, final Pageable pageable);

}
