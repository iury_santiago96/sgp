package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Endereco;
import br.com.sgp.sgp.repository.EnderecoRepository;

@Service
public class EnderecoService {

    private final EnderecoRepository enderecoRepository;

    public EnderecoService(final EnderecoRepository enderecoRepository) {
        this.enderecoRepository = enderecoRepository;
    }

    public List<Endereco> findAll() {
        return enderecoRepository.findAll();
    }

    public Optional<Endereco> findById(final Long id) {
        return enderecoRepository.findById(id);
    }

    public Endereco save(final Endereco endereco) {
        return enderecoRepository.save(endereco);
    }

    public void deleteById(final Long id) {
        enderecoRepository.deleteById(id);
    }
}
