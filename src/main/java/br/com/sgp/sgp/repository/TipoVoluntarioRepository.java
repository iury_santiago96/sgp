package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.TipoVoluntario;

@Repository
public interface TipoVoluntarioRepository extends JpaRepository<TipoVoluntario, Long> {

    List<TipoVoluntario> findAllByTipoEqualsIgnoreCase(final String tipo);

    @Query("SELECT tv FROM TipoVoluntario tv WHERE tv.tipo LIKE %:searchTerm% OR tv.descricao LIKE %:searchTerm%")
    Page<TipoVoluntario> findBySearch(@Param("searchTerm") final String searchTerm, final Pageable pageable);
}