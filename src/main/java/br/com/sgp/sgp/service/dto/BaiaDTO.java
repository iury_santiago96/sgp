package br.com.sgp.sgp.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BaiaDTO {

    private Long id;
    private String nome;
    private Integer capacidade;

}
