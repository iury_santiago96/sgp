package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Conta;
import br.com.sgp.sgp.service.ContaService;
import br.com.sgp.sgp.service.dto.ContaDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/conta")
public class ContaResource {

    private static final String ENTITY_NAME = "Conta";
    private static final String BASE_URI = "/api/conta/";

    private final ContaService contaService;

    public ContaResource(final ContaService contaService) {
        this.contaService = contaService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ContaDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(contaService.findAll(), ContaDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Conta>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(contaService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Conta>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(contaService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ContaDTO> findById(@PathVariable final Long id) {
        return contaService.findById(id)
                .map(conta -> ResponseEntity.ok(ObjectMapperUtils.map(conta, ContaDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid ContaDTO contaDTO) throws URISyntaxException {
        final Conta conta = contaService.save(ObjectMapperUtils.map(contaDTO, new Conta()));
        return ResponseEntity.created(new URI(BASE_URI + conta.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(conta.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid ContaDTO contaDTO) {
        final Conta conta = contaService.save(ObjectMapperUtils.map(contaDTO, new Conta()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(conta.getId()))).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Conta> conta = contaService.findById(id);
        if (conta.isPresent()) {
            try {
                contaService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
