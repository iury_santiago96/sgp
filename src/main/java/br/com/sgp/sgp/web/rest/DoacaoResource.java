package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Doacao;
import br.com.sgp.sgp.service.DoacaoService;
import br.com.sgp.sgp.service.dto.DoacaoDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/doacao")
public class DoacaoResource {

    private static final String ENTITY_NAME = "Doacao";
    private static final String BASE_URI = "/api/doacao/";

    private final DoacaoService doacaoService;

    public DoacaoResource(final DoacaoService doacaoService) {
        this.doacaoService = doacaoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<DoacaoDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(doacaoService.findAll(), DoacaoDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Doacao>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(doacaoService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Doacao>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(doacaoService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DoacaoDTO> findById(@PathVariable final Long id) {
        return doacaoService.findById(id)
                .map(doacao -> ResponseEntity.ok(ObjectMapperUtils.map(doacao, DoacaoDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid DoacaoDTO doacaoDTO) throws URISyntaxException {
        final Doacao doacao = doacaoService.save(ObjectMapperUtils.map(doacaoDTO, new Doacao()));
        return ResponseEntity.created(new URI(BASE_URI + doacao.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(doacao.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid DoacaoDTO doacaoDTO) {
        final Doacao doacao = doacaoService.save(ObjectMapperUtils.map(doacaoDTO, new Doacao()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(doacao.getId()))).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Doacao> doacao = doacaoService.findById(id);
        if (doacao.isPresent()) {
            try {
                doacaoService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
