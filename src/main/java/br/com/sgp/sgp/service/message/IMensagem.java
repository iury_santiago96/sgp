package br.com.sgp.sgp.service.message;

public interface IMensagem {
    String getTipo();
    String getDescricao();
}
