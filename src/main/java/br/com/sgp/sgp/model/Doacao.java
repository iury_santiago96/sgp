package br.com.sgp.sgp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Doacao extends BasicModel {

    @Column(length = 9999999)
    private String descricao;
    private BigDecimal quantidade;
    private Date dtDoacao;
    @ManyToOne
    @JoinColumn(name = "id_voluntario")
    private Voluntario voluntario;
}
