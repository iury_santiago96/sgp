package br.com.sgp.sgp.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Endereco extends BasicModel {

    private String rua;
    private String numero;
    private String bairro;
    private String cidade;
    private String municipio;
}
