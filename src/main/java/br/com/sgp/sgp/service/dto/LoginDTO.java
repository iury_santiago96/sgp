package br.com.sgp.sgp.service.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LoginDTO {

    private @NotNull @NotBlank String username;
    private @NotNull @NotBlank String password;

}

