package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Usuario;
import br.com.sgp.sgp.service.UsuarioService;
import br.com.sgp.sgp.service.dto.UsuarioDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    private static final String ENTITY_NAME = "Usuario";
    private static final String BASE_URI = "/api/usuario/";

    private final UsuarioService usuarioService;

    public UsuarioResource(final UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<UsuarioDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(usuarioService.findAll(), UsuarioDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Usuario>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(usuarioService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Usuario>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(usuarioService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioDTO> findById(@PathVariable final Long id) {
        return usuarioService.findById(id)
                .map(usuario -> ResponseEntity.ok(ObjectMapperUtils.map(usuario, UsuarioDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid UsuarioDTO usuarioDTO) throws URISyntaxException {
        final List<Usuario> logins = usuarioService.findAllByLoginEqualsIgnoreCase(usuarioDTO.getLogin());

        if (logins.isEmpty()) {
            final Usuario usuario = usuarioService.save(ObjectMapperUtils.map(usuarioDTO, new Usuario()));
            return ResponseEntity.created(new URI(BASE_URI + usuario.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(usuario.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um usuário com o mesmo login no sistema!")).build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid UsuarioDTO usuarioDTO) {
        final List<Usuario> logins = usuarioService.findAllByLoginEqualsIgnoreCase(usuarioDTO.getLogin());

        if (logins.isEmpty() || logins.stream().allMatch(it -> it.getId().equals(usuarioDTO.getId()))) {
            final Usuario usuario = usuarioService.save(ObjectMapperUtils.map(usuarioDTO, new Usuario()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(usuario.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um usuário com o mesmo login no sistema!")).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Usuario> usuario = usuarioService.findById(id);
        if (usuario.isPresent()) {
            try {
                usuarioService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}