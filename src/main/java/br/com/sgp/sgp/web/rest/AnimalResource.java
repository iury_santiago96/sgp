package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Animal;
import br.com.sgp.sgp.service.AnimalService;
import br.com.sgp.sgp.service.dto.AnimalDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/animal")
public class AnimalResource {

    private static final String ENTITY_NAME = "Animal";
    private static final String BASE_URI = "/api/animal/";

    private final AnimalService animalService;

    public AnimalResource(final AnimalService animalService) {
        this.animalService = animalService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<AnimalDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(animalService.findAll(), AnimalDTO.class)));
    }

    @GetMapping("/animais/ativos")
    public ResponseEntity<List<AnimalDTO>> findByStatusEqualsOrStatusNull() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(animalService.findByStatusEqualsOrStatusNull(), AnimalDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Animal>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(animalService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Animal>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(animalService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnimalDTO> findById(@PathVariable final Long id) {
        return animalService.findById(id)
                .map(animal -> ResponseEntity.ok(ObjectMapperUtils.map(animal, AnimalDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid AnimalDTO animalDTO) throws URISyntaxException {
        final Animal animal = animalService.save(ObjectMapperUtils.map(animalDTO, new Animal()));
        return ResponseEntity.created(new URI(BASE_URI + animal.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(animal.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid AnimalDTO animalDTO) {
        final Animal animal = animalService.save(ObjectMapperUtils.map(animalDTO, new Animal()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(animal.getId())))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Animal> animal = animalService.findById(id);
        if (animal.isPresent()) {
            try {
                animalService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
