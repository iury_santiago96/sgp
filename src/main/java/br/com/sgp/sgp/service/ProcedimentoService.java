package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Procedimento;
import br.com.sgp.sgp.repository.ProcedimentoRepository;

@Service
public class ProcedimentoService {

    private final ProcedimentoRepository procedimentoRepository;

    public ProcedimentoService(final ProcedimentoRepository procedimentoRepository) {
        this.procedimentoRepository = procedimentoRepository;
    }

    public List<Procedimento> findAll() {
        return procedimentoRepository.findAll();
    }

    public Page<Procedimento> findAllPageable(final String searchTerm, final Pageable pageable) {
        return procedimentoRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Procedimento> findById(final Long id) {
        return procedimentoRepository.findById(id);
    }

    public Procedimento save(final Procedimento procedimento) {
        return procedimentoRepository.save(procedimento);
    }

    public void deleteById(final Long id) {
        procedimentoRepository.deleteById(id);
    }

    public List<Procedimento> findAllByTipoEqualsIgnoreCase(final String tipo) {
        return procedimentoRepository.findAllByTipoEqualsIgnoreCase(tipo);
    }
}
