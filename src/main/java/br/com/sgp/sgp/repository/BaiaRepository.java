package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Baia;

@Repository
public interface BaiaRepository extends JpaRepository<Baia, Long> {

    List<Baia> findAllByNomeEqualsIgnoreCase(final String nome);

    @Query("SELECT b FROM Baia b WHERE b.nome LIKE %?1%")
    Page<Baia> findBySearch(final String searchTerm, final Pageable pageable);
}
