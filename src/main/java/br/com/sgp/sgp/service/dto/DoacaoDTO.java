package br.com.sgp.sgp.service.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DoacaoDTO {

    private Long id;
    private String descricao;
    private BigDecimal quantidade;
    private Date dtDoacao;
    private VoluntarioDTO voluntario;
}
