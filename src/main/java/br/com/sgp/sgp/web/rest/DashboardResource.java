package br.com.sgp.sgp.web.rest;

import java.time.LocalDate;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.service.AdocaoService;
import br.com.sgp.sgp.service.AnimalService;
import br.com.sgp.sgp.service.ContaService;
import br.com.sgp.sgp.service.DoacaoService;
import br.com.sgp.sgp.service.FichaAnimalService;
import br.com.sgp.sgp.service.VoluntarioService;
import br.com.sgp.sgp.service.dto.DashboardContaDTO;
import br.com.sgp.sgp.service.dto.DashboardDTO;

@RestController
@RequestMapping("/dashboard")
public class DashboardResource {

    private final AnimalService animalService;
    private final DoacaoService doacaoService;
    private final FichaAnimalService fichaAnimalService;
    private final VoluntarioService voluntarioService;
    private final ContaService contaService;
    private final AdocaoService adocaoService;

    public DashboardResource(final AnimalService animalService, final DoacaoService doacaoService,
                             final FichaAnimalService fichaAnimalService, final VoluntarioService voluntarioService,
                             final ContaService contaService, final AdocaoService adocaoService) {
        this.animalService = animalService;
        this.doacaoService = doacaoService;
        this.fichaAnimalService = fichaAnimalService;
        this.voluntarioService = voluntarioService;
        this.contaService = contaService;
        this.adocaoService = adocaoService;
    }

    @GetMapping
    public ResponseEntity<DashboardDTO> findAll() {
        final DashboardDTO result = new DashboardDTO()
                .totalAnimais(animalService.countAnimais())
                .totalAnimaisAdotados(animalService.countAnimalsByStatus())
                .totalDoacoes(doacaoService.countDoacoes())
                .totalVoluntarios(voluntarioService.countVoluntarios())
                .totalFichas(fichaAnimalService.countFichas())
                .totalAnimaisPorBaia(animalService.findAllGroupedBaia())
                .topDezAdotantes(adocaoService.findTopDez())
                .graficoConta(new DashboardContaDTO().datasets(contaService.findContasByYear(String.valueOf(LocalDate.now().getYear()))));
        return ResponseEntity.ok(result);
    }
}
