package br.com.sgp.sgp.service.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardContaDTO implements Serializable {

    private static final long serialVersionUID = -4597689971413718533L;
    private List<String> labels;
    private List<DatasetContaDTO> datasets;

    public DashboardContaDTO() {
        labels =
                Arrays.asList("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outrubro",
                              "Novembro", "Dezembro");
        datasets = Collections.emptyList();
    }

    public DashboardContaDTO labels(final List<String> labels) {
        this.labels = labels;
        return this;
    }

    public DashboardContaDTO datasets(final List<DatasetContaDTO> datasets) {
        this.datasets = datasets;
        return this;
    }
}
