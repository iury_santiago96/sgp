package br.com.sgp.sgp.security.jwt.service;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.security.jwt.TokenProvider;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {

    private static final long EXPIRATIONTIME = 864000000;
    private static final String TOKEN_PREFIX = "Bearer";
    private static final String HEADER_STRING = "Authorization";

    public static void addAuthentication(final HttpServletResponse res, final String username) {
        final String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, TokenProvider.secretKey)
                .compact();

        final String token = TOKEN_PREFIX + " " + JWT;
        res.addHeader(HEADER_STRING, token);

        try {
            res.getOutputStream().print(token);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public static Authentication getByToken(final String token) {
        System.out.println(Jwts.parser()
                                   .setSigningKey(TokenProvider.secretKey)
                                   .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                                   .getBody()
                                   .getSubject());
        final String user = Jwts.parser()
                .setSigningKey(TokenProvider.secretKey)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();

        return user != null ? new UsernamePasswordAuthenticationToken(user, null, null) : null;
    }

    public static Authentication getAuthentication(final HttpServletRequest request) {
        final String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            return getByToken(token);
        }
        return null;
    }

}
