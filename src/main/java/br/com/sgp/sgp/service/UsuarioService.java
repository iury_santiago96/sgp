package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Usuario;
import br.com.sgp.sgp.repository.UsuarioRepository;
import br.com.sgp.sgp.service.message.MensagemDTO;

@Service
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;
    private final PasswordEncoder passwordEncoder;

    public UsuarioService(final UsuarioRepository usuarioRepository, final MensagemDTO mensagemDTO,
                          final PasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }

    public Page<Usuario> findAllPageable(final String searchTerm, final Pageable pageable) {
        return usuarioRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Usuario> findById(final Long id) {
        return usuarioRepository.findById(id);
    }

    public Usuario save(final Usuario usuario) {
        usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
        return usuarioRepository.save(usuario);
    }

    public void deleteById(final Long id) {
        usuarioRepository.deleteById(id);
    }

    public Optional<Usuario> findUsuarioByLogin(final String login){
        return usuarioRepository.findOneByLogin(login);
    }

    public List<Usuario> findAllByLoginEqualsIgnoreCase(final String login) {
        return usuarioRepository.findAllByLoginEqualsIgnoreCase(login);
    }
}
