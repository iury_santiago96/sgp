package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.TipoConta;
import br.com.sgp.sgp.repository.TipoContaRepository;

@Service
public class TipoContaService {

    private final TipoContaRepository tipoContaRepository;

    public TipoContaService(final TipoContaRepository tipoContaRepository) {
        this.tipoContaRepository = tipoContaRepository;
    }

    public List<TipoConta> findAll() {
        return tipoContaRepository.findAll();
    }

    public Page<TipoConta> findAllPageable(final String searchTerm, final Pageable pageable) {
        return tipoContaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<TipoConta> findById(final Long id) {
        return tipoContaRepository.findById(id);
    }

    public TipoConta save(final TipoConta tipoConta) {
        return tipoContaRepository.save(tipoConta);
    }

    public void deleteById(final Long id) {
        tipoContaRepository.deleteById(id);
    }

    public List<TipoConta> findAllByTipoEqualsIgnoreCase(final String tipo) {
        return tipoContaRepository.findAllByTipoEqualsIgnoreCase(tipo);
    }
}
