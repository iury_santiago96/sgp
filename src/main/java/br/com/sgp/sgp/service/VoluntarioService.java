package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Voluntario;
import br.com.sgp.sgp.repository.VoluntarioRepository;

@Service
public class VoluntarioService {

    private final VoluntarioRepository voluntarioRepository;

    public VoluntarioService(final VoluntarioRepository voluntarioRepository) {
        this.voluntarioRepository = voluntarioRepository;
    }

    public List<Voluntario> findAll() {
        return voluntarioRepository.findAll();
    }

    public Page<Voluntario> findAllPageable(final String searchTerm, final Pageable pageable) {
        return voluntarioRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Voluntario> findById(final Long id) {
        return voluntarioRepository.findById(id);
    }

    public Voluntario save(final Voluntario voluntario) {
        return voluntarioRepository.save(voluntario);
    }

    public void deleteById(final Long id) {
        voluntarioRepository.deleteById(id);
    }

    public List<Voluntario> findAllByRgEqualsIgnoreCase(final String rg) {
        return voluntarioRepository.findAllByRgEqualsIgnoreCase(rg);
    }

    public List<Voluntario> findAllByCpfEqualsIgnoreCase(final String cpf) {
        return voluntarioRepository.findAllByCpfEqualsIgnoreCase(cpf);
    }

    public Long countVoluntarios() {
        return voluntarioRepository.count();
    }
}
