package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.TipoVacina;
import br.com.sgp.sgp.service.TipoVacinaService;
import br.com.sgp.sgp.service.dto.TipoVacinaDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/tipo-vacina")
public class TipoVacinaResource {

    private static final String ENTITY_NAME = "Tipo de Vacina";
    private static final String BASE_URI = "/api/tipo-vacina/";

    private final TipoVacinaService tipoVacinaService;

    public TipoVacinaResource(final TipoVacinaService tipoVacinaService) {
        this.tipoVacinaService = tipoVacinaService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<TipoVacinaDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(tipoVacinaService.findAll(), TipoVacinaDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<TipoVacina>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoVacinaService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<TipoVacina>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoVacinaService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoVacinaDTO> findById(@PathVariable final Long id) {
        return tipoVacinaService.findById(id)
                .map(vacina -> ResponseEntity.ok(ObjectMapperUtils.map(vacina, TipoVacinaDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid TipoVacinaDTO tipoVacinaDTO) throws URISyntaxException {
        final List<TipoVacina> tipos = tipoVacinaService.findAllByTipoEqualsIgnoreCase(tipoVacinaDTO.getTipo());

        if (tipos.isEmpty()) {
            final TipoVacina tipoVacina = tipoVacinaService.save(ObjectMapperUtils.map(tipoVacinaDTO, new TipoVacina()));
            return ResponseEntity.created(new URI(BASE_URI + tipoVacina.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(tipoVacina.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um tipo de vacina com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid TipoVacinaDTO tipoVacinaDTO) {
        final List<TipoVacina> tipos = tipoVacinaService.findAllByTipoEqualsIgnoreCase(tipoVacinaDTO.getTipo());

        if (tipos.isEmpty() || tipos.stream().allMatch(it -> it.getId().equals(tipoVacinaDTO.getId()))) {
            final TipoVacina tipoVacina = tipoVacinaService.save(ObjectMapperUtils.map(tipoVacinaDTO, new TipoVacina()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(tipoVacina.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um tipo de vacina com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<TipoVacina> tipoVacina = tipoVacinaService.findById(id);
        if (tipoVacina.isPresent()) {
            try {
                tipoVacinaService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
