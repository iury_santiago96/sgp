package br.com.sgp.sgp.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TipoContaDTO {

    private Long id;
    private String tipo;
    private String descricao;
}
