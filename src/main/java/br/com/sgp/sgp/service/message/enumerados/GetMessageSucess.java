package br.com.sgp.sgp.service.message.enumerados;

import br.com.sgp.sgp.service.message.IMensagem;

public enum GetMessageSucess implements IMensagem {
    REGISTRO_SUCESSO("Registro salvo com sucesso!"),
    REGISTRO_UPDATE_SUCESSO("Registro alterado com sucesso!"),
    REGISTRO_DELETE_SUCESSO("Registro deletado com sucesso!");

    String descricao;

    GetMessageSucess(final String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getTipo() {
        return "SUCESSO";
    }

    @Override
    public String getDescricao() {
        return descricao;
    }
}
