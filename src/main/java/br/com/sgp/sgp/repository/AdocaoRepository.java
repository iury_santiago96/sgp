package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Adocao;
import br.com.sgp.sgp.service.dto.AdotantesAnimaisDTO;

@Repository
public interface AdocaoRepository extends JpaRepository<Adocao, Long> {

    @Query("SELECT A FROM Adocao A WHERE A.adotante.nome LIKE %?1% OR A.voluntario.nome LIKE %?1%")
    Page<Adocao> findBySearch(final String searchTerm, final Pageable pageable);

    @Query("SELECT new br.com.sgp.sgp.service.dto.AdotantesAnimaisDTO(count(a3.id), a2.nome) FROM Adocao as a "
           + "INNER JOIN a.adotante as a2 "
           + "INNER JOIN a.animais as a3 "
           + "GROUP BY a2.id")
    List<AdotantesAnimaisDTO> findTopDez();
}
