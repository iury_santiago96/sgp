package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Vacina;
import br.com.sgp.sgp.repository.VacinaRepository;

@Service
public class VacinaService {

    private final VacinaRepository vacinaRepository;

    public VacinaService(final VacinaRepository vacinaRepository) {
        this.vacinaRepository = vacinaRepository;
    }

    public List<Vacina> findAll() {
        return vacinaRepository.findAll();
    }

    public Page<Vacina> findAllPageable(final String searchTerm, final Pageable pageable) {
        return vacinaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Vacina> findById(final Long id) {
        return vacinaRepository.findById(id);
    }

    public Vacina save(final Vacina vacina) {
        return vacinaRepository.save(vacina);
    }

    public void deleteById(final Long id) {
        vacinaRepository.deleteById(id);
    }

}
