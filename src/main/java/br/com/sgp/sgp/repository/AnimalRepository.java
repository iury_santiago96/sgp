package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Animal;
import br.com.sgp.sgp.service.dto.AnimaisBaiaDTO;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    List<Animal> findByStatusEqualsOrStatusNull(final Boolean status);

    @Query("SELECT A FROM Animal A WHERE A.nome LIKE %?1% OR A.raca.nome LIKE %?1% OR A.raca.nome LIKE %?1%")
    Page<Animal> findBySearch(final String searchTerm, final Pageable pageable);

    Long countAnimalsByStatus(final Boolean status);

    @Query("SELECT new br.com.sgp.sgp.service.dto.AnimaisBaiaDTO(count(a.id), b.nome, b.capacidade) FROM Animal AS a INNER JOIN a.baia AS b GROUP BY b.id ORDER BY b.nome")
    List<AnimaisBaiaDTO> findAllGroupedBaia();
}
