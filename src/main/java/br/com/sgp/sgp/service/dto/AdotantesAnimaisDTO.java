package br.com.sgp.sgp.service.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AdotantesAnimaisDTO implements Serializable {

    private static final long serialVersionUID = -4879673672324659221L;
    private Long total;
    private String nome;

    public AdotantesAnimaisDTO(Long total, String nome) {
        this.total = total;
        this.nome = nome;
    }
}
