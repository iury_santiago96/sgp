package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.Voluntario;
import br.com.sgp.sgp.service.VoluntarioService;
import br.com.sgp.sgp.service.dto.VoluntarioDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/voluntario")
public class VoluntarioResource {

    private static final String ENTITY_NAME = "Voluntario";
    private static final String BASE_URI = "/api/voluntario/";

    private final VoluntarioService voluntarioService;

    public VoluntarioResource(final VoluntarioService voluntarioService) {
        this.voluntarioService = voluntarioService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<VoluntarioDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(voluntarioService.findAll(), VoluntarioDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<Voluntario>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(voluntarioService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<Voluntario>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(voluntarioService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<VoluntarioDTO> findById(@PathVariable final Long id) {
        return voluntarioService.findById(id)
                .map(voluntario -> ResponseEntity.ok(ObjectMapperUtils.map(voluntario, VoluntarioDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid VoluntarioDTO voluntarioDTO) throws URISyntaxException {
        final List<Voluntario> cpfs = voluntarioService.findAllByCpfEqualsIgnoreCase(voluntarioDTO.getCpf());
        final List<Voluntario> rgs = voluntarioService.findAllByRgEqualsIgnoreCase(voluntarioDTO.getRg());

        if (cpfs.isEmpty() && rgs.isEmpty()) {
            final Voluntario voluntario = voluntarioService.save(ObjectMapperUtils.map(voluntarioDTO, new Voluntario()));
            return ResponseEntity.created(new URI(BASE_URI + voluntario.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(voluntario.getId()))).build();
        } else {
            if (!cpfs.isEmpty() && !rgs.isEmpty()) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo rg e cpf no sistema!"))
                        .build();
            } else if (rgs.isEmpty()) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo cpf no sistema!"))
                        .build();
            } else {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo rg no sistema!"))
                        .build();
            }
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid VoluntarioDTO voluntarioDTO) {
        final List<Voluntario> cpfs = voluntarioService.findAllByCpfEqualsIgnoreCase(voluntarioDTO.getCpf());
        final List<Voluntario> rgs = voluntarioService.findAllByRgEqualsIgnoreCase(voluntarioDTO.getRg());

        if (cpfs.isEmpty() && rgs.isEmpty() || cpfs.stream().allMatch(it -> it.getId().equals(voluntarioDTO.getId())) && rgs.stream()
                .allMatch(it -> it.getId().equals(voluntarioDTO.getId()))) {
            final Voluntario voluntario = voluntarioService.save(ObjectMapperUtils.map(voluntarioDTO, new Voluntario()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(voluntario.getId())))
                    .build();
        } else {
            if (!cpfs.isEmpty() && !rgs.isEmpty() && !cpfs.stream().allMatch(it -> it.getId().equals(voluntarioDTO.getId()))
                && !rgs.stream().allMatch(it -> it.getId().equals(voluntarioDTO.getId()))) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo rg e cpf no sistema!"))
                        .build();
            } else if (!rgs.isEmpty() && !rgs.stream().allMatch(it -> it.getId().equals(voluntarioDTO.getId()))) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo rg no sistema!"))
                        .build();
            } else {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um voluntário com o mesmo cpf no sistema!"))
                        .build();
            }
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<Voluntario> voluntario = voluntarioService.findById(id);
        if (voluntario.isPresent()) {
            try {
                voluntarioService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
