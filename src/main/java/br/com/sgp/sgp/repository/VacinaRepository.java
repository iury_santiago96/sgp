package br.com.sgp.sgp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Vacina;

@Repository
public interface VacinaRepository extends JpaRepository<Vacina, Long> {

    @Query("SELECT v FROM Vacina v WHERE v.nome LIKE %?1% OR v.fabricante LIKE %?1% OR v.marca LIKE %?1% OR v.lote LIKE %?1% OR v.tipoVacina.tipo LIKE %?1%")
    Page<Vacina> findBySearch(final String searchTerm, final Pageable pageable);
}
