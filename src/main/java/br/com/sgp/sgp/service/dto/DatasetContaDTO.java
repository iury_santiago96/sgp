package br.com.sgp.sgp.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DatasetContaDTO implements Serializable {

    private static final long serialVersionUID = -5817969631804387390L;
    private String label;
    private boolean fill;
    private Double lineTension;
    private String backgroundColor;
    private String borderColor;
    private String borderCapStyle;
    private String borderJoinStyle;
    private String pointBorderColor;
    private String pointBackgroundColor;
    private Integer pointBorderWidth;
    private Integer pointHoverRadius;
    private String pointHoverBackgroundColor;
    private String pointHoverBorderColor;
    private Integer pointHoverBorderWidth;
    private Integer pointRadius;
    private Integer pointHitRadius;
    private List<BigDecimal> data;

    public DatasetContaDTO(final String label, final String backgroundColor, final String borderColor, final String pointBorderColor,
                           final String pointBackgroundColor, final String pointHoverBackgroundColor,
                           final String pointHoverBorderColor) {
        this.label = label;
        fill = false;
        lineTension = 0.1;
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
        borderCapStyle = "butt";
        borderJoinStyle = "miter";
        this.pointBorderColor = pointBorderColor;
        this.pointBackgroundColor = pointBackgroundColor;
        pointBorderWidth = 1;
        pointHoverRadius = 5;
        this.pointHoverBackgroundColor = pointHoverBackgroundColor;
        this.pointHoverBorderColor = pointHoverBorderColor;
        pointHoverBorderWidth = 2;
        pointRadius = 1;
        pointHitRadius = 10;
    }

    public DatasetContaDTO label(final String label) {
        this.label = label;
        return this;
    }

    public DatasetContaDTO fill(final boolean fill) {
        this.fill = fill;
        return this;
    }

    public DatasetContaDTO lineTension(final Double lineTension) {
        this.lineTension = lineTension;
        return this;
    }

    public DatasetContaDTO backgroundColor(final String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public DatasetContaDTO borderColor(final String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public DatasetContaDTO borderCapStyle(final String borderCapStyle) {
        this.borderCapStyle = borderCapStyle;
        return this;
    }

    public DatasetContaDTO borderJoinStyle(final String borderJoinStyle) {
        this.borderJoinStyle = borderJoinStyle;
        return this;
    }

    public DatasetContaDTO pointBorderColor(final String pointBorderColor) {
        this.pointBorderColor = pointBorderColor;
        return this;
    }

    public DatasetContaDTO pointBackgroundColor(final String pointBackgroundColor) {
        this.pointBackgroundColor = pointBackgroundColor;
        return this;
    }

    public DatasetContaDTO pointBorderWidth(final Integer pointBorderWidth) {
        this.pointBorderWidth = pointBorderWidth;
        return this;
    }

    public DatasetContaDTO pointHoverRadius(final Integer pointHoverRadius) {
        this.pointHoverRadius = pointHoverRadius;
        return this;
    }

    public DatasetContaDTO pointHoverBackgroundColor(final String pointHoverBackgroundColor) {
        this.pointHoverBackgroundColor = pointHoverBackgroundColor;
        return this;
    }

    public DatasetContaDTO pointHoverBorderColor(final String pointHoverBorderColor) {
        this.pointHoverBorderColor = pointHoverBorderColor;
        return this;
    }

    public DatasetContaDTO pointHoverBorderWidth(final Integer pointHoverBorderWidth) {
        this.pointHoverBorderWidth = pointHoverBorderWidth;
        return this;
    }

    public DatasetContaDTO pointRadius(final Integer pointRadius) {
        this.pointRadius = pointRadius;
        return this;
    }

    public DatasetContaDTO pointHitRadius(final Integer pointHitRadius) {
        this.pointHitRadius = pointHitRadius;
        return this;
    }

    public DatasetContaDTO data(final List<BigDecimal> data) {
        this.data = data;
        return this;
    }
}
