package br.com.sgp.sgp.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AdotanteDTO {

    private Long id;
    private String nome;
    private String rg;
    private String cpf;
    private String telefone;
    private String observacao;
    private EnderecoDTO endereco;
}