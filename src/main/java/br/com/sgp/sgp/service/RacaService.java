package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Raca;
import br.com.sgp.sgp.repository.RacaRepository;

@Service
public class RacaService {

    private final RacaRepository racaRepository;

    public RacaService(final RacaRepository racaRepository) {
        this.racaRepository = racaRepository;
    }

    public List<Raca> findAll() {
        return racaRepository.findAll();
    }

    public Page<Raca> findAllPageable(final String searchTerm, final Pageable pageable) {
        return racaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Raca> findById(final Long id) {
        return racaRepository.findById(id);
    }

    public Raca save(final Raca raca) {
        return racaRepository.save(raca);
    }

    public void deleteById(final Long id) {
        racaRepository.deleteById(id);
    }

    public List<Raca> findAllByNomeEqualsIgnoreCase(final String nome) {
        return racaRepository.findAllByNomeEqualsIgnoreCase(nome);
    }
}
