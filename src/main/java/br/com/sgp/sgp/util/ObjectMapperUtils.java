package br.com.sgp.sgp.util;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public final class ObjectMapperUtils {

    private static final ModelMapper MAPPER = new ModelMapper();

    static {
        MAPPER.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    private ObjectMapperUtils() {
    }

    public static <D, T> D map(final T entity, final Class<D> outClass) {
        return MAPPER.map(entity, outClass);
    }

    public static <D, T> List<D> mapAll(final Collection<T> entityList, final Class<D> outCLass) {
        return entityList.stream()
                .map(entity -> map(entity, outCLass))
                .collect(Collectors.toList());
    }

    /**
     * Maps {@code source} to {@code destination}.
     *
     * @param source      object to map from
     * @param destination object to map to
     */
    public static <S, D> D map(final S source, final D destination) {
        MAPPER.map(source, destination);
        return destination;
    }
}
