package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Raca;

@Repository
public interface RacaRepository extends JpaRepository<Raca, Long> {

    List<Raca> findAllByNomeEqualsIgnoreCase(final String nome);

    @Query("SELECT r FROM Raca r WHERE r.nome LIKE %?1% OR r.descricao LIKE %?1%")
    Page<Raca> findBySearch(final String searchTerm, final Pageable pageable);
}
