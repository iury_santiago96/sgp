package br.com.sgp.sgp.service.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class AdocaoDTO {

    private Long id;
    private Date dtAdocao;
    private VoluntarioDTO voluntario;
    private AdotanteDTO adotante;
    private List<AnimalDTO> animais;

}
