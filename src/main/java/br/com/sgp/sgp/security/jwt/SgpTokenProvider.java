package br.com.sgp.sgp.security.jwt;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.java.Log;

@Log
@Component
public class SgpTokenProvider {

    private String secretKey;

    private long tokenValidityInMilliseconds;

    @PostConstruct
    public void init() {
        secretKey = "MinhaFraseSuperSeCrEta";
        tokenValidityInMilliseconds = 3153600000L;
    }

    public String createToken(final String dados) {
        final long now = new Date().getTime();
        final Date validity = new Date(now + tokenValidityInMilliseconds);
        return Jwts.builder()
                .setSubject(dados)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(validity)
                .compact();
    }

    public String getDados(final String token) {
        final Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    public boolean validateToken(final String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (final SignatureException e) {
            log.info("Invalid JWT signature.");
        } catch (final MalformedJwtException e) {
            log.info("Invalid JWT token.");
        } catch (final ExpiredJwtException e) {
            log.info("Expired JWT token.");
        } catch (final UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
        } catch (final IllegalArgumentException e) {
            log.info("JWT token compact of handler are invalid.");
        }
        return false;
    }
}
