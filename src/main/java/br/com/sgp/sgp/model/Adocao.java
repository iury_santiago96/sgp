package br.com.sgp.sgp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Adocao extends BasicModel {

    private Date dtAdocao;
    @ManyToOne
    @JoinColumn(name = "id_voluntario")
    private Voluntario voluntario;
    @ManyToOne
    @JoinColumn(name = "id_adotante")
    private Adotante adotante;
    @OneToMany(mappedBy = "adocao")
    private List<Animal> animais;

}
