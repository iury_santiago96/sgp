package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.TipoVoluntario;
import br.com.sgp.sgp.repository.TipoVoluntarioRepository;

@Service
public class TipoVoluntarioService {

    private final TipoVoluntarioRepository tipoVoluntarioRepository;

    public TipoVoluntarioService(final TipoVoluntarioRepository tipoVoluntarioRepository) {
        this.tipoVoluntarioRepository = tipoVoluntarioRepository;
    }

    public List<TipoVoluntario> findAll() {
        return tipoVoluntarioRepository.findAll();
    }

    public Page<TipoVoluntario> findAllPageable(final String searchTerm, final Pageable pageable) {
        return tipoVoluntarioRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<TipoVoluntario> findById(final Long id) {
        return tipoVoluntarioRepository.findById(id);
    }

    public TipoVoluntario save(final TipoVoluntario tipoVoluntario) {
        return tipoVoluntarioRepository.save(tipoVoluntario);
    }

    public void deleteById(final Long id) {
        tipoVoluntarioRepository.deleteById(id);
    }

    public List<TipoVoluntario> findAllByTipoEqualsIgnoreCase(final String tipo) {
        return tipoVoluntarioRepository.findAllByTipoEqualsIgnoreCase(tipo);
    }
}
