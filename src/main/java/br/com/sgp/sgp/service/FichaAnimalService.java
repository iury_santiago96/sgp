package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.FichaAnimal;
import br.com.sgp.sgp.repository.FichaAnimalRepository;

@Service
public class FichaAnimalService {

    private final FichaAnimalRepository fichaAnimalRepository;

    public FichaAnimalService(final FichaAnimalRepository fichaAnimalRepository) {
        this.fichaAnimalRepository = fichaAnimalRepository;
    }
    public List<FichaAnimal> findAll() {
        return fichaAnimalRepository.findAll();
    }

    public Page<FichaAnimal> findAllPageable(final String searchTerm, final Pageable pageable) {
        return fichaAnimalRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<FichaAnimal> findById(final Long id) {
        return fichaAnimalRepository.findById(id);
    }

    public FichaAnimal save(final FichaAnimal fichaAnimal) {
        return fichaAnimalRepository.save(fichaAnimal);
    }

    public void deleteById(final Long id) {
        fichaAnimalRepository.deleteById(id);
    }

    public Long countFichas() {
        return fichaAnimalRepository.count();
    }

    public List<FichaAnimal> findAllByAnimalId(final Long id) {
        return fichaAnimalRepository.findAllByAnimalId(id);
    }
}