package br.com.sgp.sgp.service.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FichaAnimalDTO {

    private Long id;
    private String observacao;
    private Date data;
    private List<VacinaDTO> vacinas;
    private List<ProcedimentoDTO> procedimentos;
    private AnimalDTO animal;
    private VoluntarioDTO voluntario;
}
