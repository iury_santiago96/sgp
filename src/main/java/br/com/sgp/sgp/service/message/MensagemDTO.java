package br.com.sgp.sgp.service.message;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Controller;

import br.com.sgp.sgp.service.message.enumerados.GetMessageError;
import br.com.sgp.sgp.service.message.enumerados.GetMessageSucess;
import lombok.Data;

@Data
@Controller
public class MensagemDTO {

    public static final String SUCESSO = "SUCESSO";
    private List<String> messageSucess = new LinkedList<>();
    private List<String> messageError = new LinkedList<>();

    public void setMessageError(final GetMessageError getMessageError){
        getMessageError().add(getMessageError.getDescricao());
    }

    public void setMessageSucess(final GetMessageSucess getMessageSucess){
        getMessageSucess().add(getMessageSucess.getDescricao());
    }

    public void setMensagem(final IMensagem iMensagem) {
        if (SUCESSO.equals(iMensagem.getTipo())) {
            getMessageSucess().add(iMensagem.getDescricao());
        } else {
            getMessageError().add(iMensagem.getDescricao());
        }
    }

    public void clear(){
        messageError.clear();
        messageSucess.clear();
    }
}
