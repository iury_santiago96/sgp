package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Adotante;
import br.com.sgp.sgp.repository.AdotanteRepository;

@Service
public class AdotanteService {

    private final AdotanteRepository adotanteRepository;

    public AdotanteService(final AdotanteRepository adotanteRepository) {
        this.adotanteRepository = adotanteRepository;
    }

    public List<Adotante> findAll() {
        return adotanteRepository.findAll();
    }

    public Page<Adotante> findAllPageable(final String searchTerm, final Pageable pageable) {
        return adotanteRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Adotante> findById(final Long id) {
        return adotanteRepository.findById(id);
    }

    public Adotante save(final Adotante adotante) {
        return adotanteRepository.save(adotante);
    }

    public void deleteById(final Long id) {
        adotanteRepository.deleteById(id);
    }

    public List<Adotante> findAllByRgEqualsIgnoreCase(final String rg) {
        return adotanteRepository.findAllByRgEqualsIgnoreCase(rg);
    }

    public List<Adotante> findAllByCpfEqualsIgnoreCase(final String cpf) {
        return adotanteRepository.findAllByCpfEqualsIgnoreCase(cpf);
    }
}