package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.FichaAnimal;
import br.com.sgp.sgp.service.FichaAnimalService;
import br.com.sgp.sgp.service.dto.FichaAnimalDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/ficha-animal")
public class FichaAnimalResource {

    private static final String ENTITY_NAME = "Ficha animal";
    private static final String BASE_URI = "/api/ficha-animal/";

    private final FichaAnimalService fichaAnimalService;

    public FichaAnimalResource(final FichaAnimalService fichaAnimalService) {
        this.fichaAnimalService = fichaAnimalService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<FichaAnimalDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(fichaAnimalService.findAll(), FichaAnimalDTO.class)));
    }

    @GetMapping("/relatorio/{id}")
    public ResponseEntity<List<FichaAnimalDTO>> findAllByAnimalId(@PathVariable final Long id) {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(fichaAnimalService.findAllByAnimalId(id), FichaAnimalDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<FichaAnimal>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(fichaAnimalService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<FichaAnimal>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(fichaAnimalService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<FichaAnimalDTO> findById(@PathVariable final Long id) {
        return fichaAnimalService.findById(id)
                .map(fichaAnimal -> ResponseEntity.ok(ObjectMapperUtils.map(fichaAnimal, FichaAnimalDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid FichaAnimalDTO fichaAnimalDTO) throws URISyntaxException {
        final FichaAnimal fichaAnimal = fichaAnimalService.save(ObjectMapperUtils.map(fichaAnimalDTO, new FichaAnimal()));
        return ResponseEntity.created(new URI(BASE_URI + fichaAnimal.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(fichaAnimal.getId()))).build();
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid FichaAnimalDTO fichaAnimalDTO) {
        final FichaAnimal fichaAnimal = fichaAnimalService.save(ObjectMapperUtils.map(fichaAnimalDTO, new FichaAnimal()));
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(fichaAnimal.getId())))
                .build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<FichaAnimal> fichaAnimal = fichaAnimalService.findById(id);
        if (fichaAnimal.isPresent()) {
            try {
                fichaAnimalService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id)))
                        .build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }

}
