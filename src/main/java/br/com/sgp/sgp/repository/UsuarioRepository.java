package br.com.sgp.sgp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    List<Usuario> findAllByLoginEqualsIgnoreCase(final String nome);

    @Query("SELECT U FROM Usuario U WHERE U.login LIKE %?1% OR U.voluntario.nome LIKE %?1%")
    Page<Usuario> findBySearch(final String searchTerm, final Pageable pageable);

    Optional<Usuario> findOneByLogin(final String login);
}
