package br.com.sgp.sgp.service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Conta;
import br.com.sgp.sgp.repository.ContaRepository;
import br.com.sgp.sgp.service.dto.ContaResultDTO;
import br.com.sgp.sgp.service.dto.DatasetContaDTO;

@Service
public class ContaService {

    private final ContaRepository contaRepository;

    public ContaService(final ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }

    public List<Conta> findAll() {
        return contaRepository.findAll();
    }

    public Page<Conta> findAllPageable(final String searchTerm, final Pageable pageable) {
        return contaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Conta> findById(final Long id) {
        return contaRepository.findById(id);
    }

    public Conta save(final Conta conta) {
        return contaRepository.save(conta);
    }

    public void deleteById(final Long id) {
        contaRepository.deleteById(id);
    }

    public List<DatasetContaDTO> findContasByYear(final String ano) {
        final List<ContaResultDTO> result = contaRepository.findContasByYear(ano);
        final List<ContaResultDTO> resto = new LinkedList<>();
        IntStream.range(1, 13)
                .forEach(it -> result.stream().collect(Collectors.groupingBy(ContaResultDTO::getTipo)).forEach((tipo, lista) -> {
                    final ContaResultDTO contaResultDTO = lista.stream()
                            .filter(contaResult -> contaResult.getMes() == it).findFirst()
                            .orElse(new ContaResultDTO(BigDecimal.ZERO, tipo, it));
                    resto.add(contaResultDTO);
                }));
        final List<DatasetContaDTO> contas = new LinkedList<>();
        resto.stream().collect(Collectors.groupingBy(ContaResultDTO::getTipo))
                .forEach((tipo, lista) -> {
                    final String[] colors = randomColor();
                    contas
                            .add(new DatasetContaDTO(tipo, colors[0], colors[1], colors[1], colors[1], colors[1], colors[0])
                                         .data(lista.stream().map(ContaResultDTO::getTotal).collect(Collectors.toList())));
                });

        return contas;
    }

    private String[] randomColor() {
        final Random random = new Random();
        final int r = random.nextInt(255);
        final int g = random.nextInt(255);
        final int b = random.nextInt(255);

        return new String[]{String.format("rgba(%s,%s,%s,%s)", r, g, b, "0.4"), String.format("rgba(%s,%s,%s,%s)", r, g, b, "1")};
    }
}
