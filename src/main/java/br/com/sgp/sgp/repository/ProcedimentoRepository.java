package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.Procedimento;

@Repository
public interface ProcedimentoRepository extends JpaRepository<Procedimento, Long> {

    List<Procedimento> findAllByTipoEqualsIgnoreCase(final String tipo);

    @Query("SELECT p FROM Procedimento p WHERE p.tipo LIKE %?1% OR p.descricao LIKE %?1%")
    Page<Procedimento> findBySearch(final String searchTerm, final Pageable pageable);
}
