package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.FichaAnimal;

@Repository
public interface FichaAnimalRepository extends JpaRepository<FichaAnimal, Long> {

    List<FichaAnimal> findAllByAnimalId(final Long id);

    @Query("SELECT Fa FROM FichaAnimal Fa WHERE Fa.observacao LIKE %?1% OR Fa.animal.nome LIKE %?1% OR Fa.voluntario.nome LIKE %?1%")
    Page<FichaAnimal> findBySearch(final String searchTerm, final Pageable pageable);
}
