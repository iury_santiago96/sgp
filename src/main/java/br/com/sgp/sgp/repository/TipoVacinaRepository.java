package br.com.sgp.sgp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sgp.sgp.model.TipoVacina;

@Repository
public interface TipoVacinaRepository extends JpaRepository<TipoVacina, Long> {

    List<TipoVacina> findAllByTipoEqualsIgnoreCase(final String tipo);

    @Query("SELECT tv FROM TipoVacina tv WHERE tv.tipo LIKE %?1% OR tv.descricao LIKE %?1%")
    Page<TipoVacina> findBySearch(final String searchTerm, final Pageable pageable);
}
