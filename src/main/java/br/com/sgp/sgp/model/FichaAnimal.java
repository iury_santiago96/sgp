package br.com.sgp.sgp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ficha_animal")
@Getter
@Setter
@NoArgsConstructor
public class FichaAnimal extends BasicModel {
    @Column(length = 9999999)
    private String observacao;
    private Date data;
    @ManyToMany
    @JoinTable(name = "ficha_animal_vacinas", joinColumns = @JoinColumn(name = "id_ficha_animal"), inverseJoinColumns = @JoinColumn(name = "id_vacina"))
    private List<Vacina> vacinas;
    @ManyToMany
    @JoinTable(name = "ficha_animal_procedimento", joinColumns = @JoinColumn(name = "id_ficha_animal"), inverseJoinColumns = @JoinColumn(name = "id_procedimento"))
    private List<Procedimento> procedimentos;
    @ManyToOne
    @JoinColumn(name = "id_animal")
    private Animal animal;
    @ManyToOne
    @JoinColumn(name = "id_voluntario")
    private Voluntario voluntario;
}
