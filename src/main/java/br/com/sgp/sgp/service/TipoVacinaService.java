package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.TipoVacina;
import br.com.sgp.sgp.repository.TipoVacinaRepository;

@Service
public class TipoVacinaService {

    private final TipoVacinaRepository tipoVacinaRepository;

    public TipoVacinaService(final TipoVacinaRepository tipoVacinaRepository) {
        this.tipoVacinaRepository = tipoVacinaRepository;
    }

    public List<TipoVacina> findAll() {
        return tipoVacinaRepository.findAll();
    }

    public Page<TipoVacina> findAllPageable(final String searchTerm, final Pageable pageable) {
        return tipoVacinaRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<TipoVacina> findById(final Long id) {
        return tipoVacinaRepository.findById(id);
    }

    public TipoVacina save(final TipoVacina tipoVacina) {
        return tipoVacinaRepository.save(tipoVacina);
    }

    public void deleteById(final Long id) {
        tipoVacinaRepository.deleteById(id);
    }

    public List<TipoVacina> findAllByTipoEqualsIgnoreCase(final String tipo) {
        return tipoVacinaRepository.findAllByTipoEqualsIgnoreCase(tipo);
    }
}
