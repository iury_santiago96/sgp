package br.com.sgp.sgp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.sgp.sgp.model.Adocao;
import br.com.sgp.sgp.repository.AdocaoRepository;
import br.com.sgp.sgp.service.dto.AdotantesAnimaisDTO;

@Service
public class AdocaoService {

    private final AdocaoRepository adocaoRepository;

    public AdocaoService(final AdocaoRepository adocaoRepository) {
        this.adocaoRepository = adocaoRepository;
    }

    public List<Adocao> findAll() {
        return adocaoRepository.findAll();
    }

    public Page<Adocao> findAllPageable(final String searchTerm, final Pageable pageable) {
        return adocaoRepository.findBySearch(searchTerm, pageable);
    }

    public Optional<Adocao> findById(final Long id) {
        return adocaoRepository.findById(id);
    }

    public Adocao save(final Adocao adocao) {
        return adocaoRepository.save(adocao);
    }

    public void deleteById(final Long id) {
        adocaoRepository.deleteById(id);
    }

    public List<AdotantesAnimaisDTO> findTopDez() {
        return adocaoRepository.findTopDez();
    }
}
