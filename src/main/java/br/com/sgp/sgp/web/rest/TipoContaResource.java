package br.com.sgp.sgp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sgp.sgp.model.TipoConta;
import br.com.sgp.sgp.service.TipoContaService;
import br.com.sgp.sgp.service.dto.TipoContaDTO;
import br.com.sgp.sgp.util.HeaderUtil;
import br.com.sgp.sgp.util.ObjectMapperUtils;

@RestController
@RequestMapping("/tipo-conta")
public class TipoContaResource {

    private static final String ENTITY_NAME = "Tipo de Conta";
    private static final String BASE_URI = "/api/tipo-conta/";

    private final TipoContaService tipoContaService;

    public TipoContaResource(final TipoContaService tipoContaService) {
        this.tipoContaService = tipoContaService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<TipoContaDTO>> findAll() {
        return ResponseEntity.of(Optional.of(ObjectMapperUtils.mapAll(tipoContaService.findAll(), TipoContaDTO.class)));
    }

    @GetMapping
    public ResponseEntity<Page<TipoConta>> findAllPageable(final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoContaService.findAllPageable("", pageable)));
    }

    @GetMapping("/pesquisa/{termo}")
    public ResponseEntity<Page<TipoConta>> search(@PathVariable final String termo, final Pageable pageable) {
        return ResponseEntity.of(Optional.of(tipoContaService.findAllPageable(termo, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TipoContaDTO> findById(@PathVariable final Long id) {
        return tipoContaService.findById(id)
                .map(conta -> ResponseEntity.ok(ObjectMapperUtils.map(conta, TipoContaDTO.class)))
                .orElseGet(
                        () -> ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build());
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody final @Valid TipoContaDTO tipoContaDTO) throws URISyntaxException {
        final List<TipoConta> tipos = tipoContaService.findAllByTipoEqualsIgnoreCase(tipoContaDTO.getTipo());

        if (tipos.isEmpty()) {
            final TipoConta tipoConta = tipoContaService.save(ObjectMapperUtils.map(tipoContaDTO, new TipoConta()));
            return ResponseEntity.created(new URI(BASE_URI + tipoConta.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, String.valueOf(tipoConta.getId()))).build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe um tipo de conta com o mesmo tipo no sistema!"))
                    .build();
        }
    }

    @PutMapping
    public ResponseEntity<Void> update(@RequestBody final @Valid TipoContaDTO tipoContaDTO) {
        final List<TipoConta> tipos = tipoContaService.findAllByTipoEqualsIgnoreCase(tipoContaDTO.getTipo());

        if (tipos.isEmpty() || tipos.stream().allMatch(it -> it.getId().equals(tipoContaDTO.getId()))) {
            final TipoConta tipoConta = tipoContaService.save(ObjectMapperUtils.map(tipoContaDTO, new TipoConta()));
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, String.valueOf(tipoConta.getId())))
                    .build();
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Já existe uma conta com o mesmo tipo no sistema!")).build();

        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        final Optional<TipoConta> tipoConta = tipoContaService.findById(id);
        if (tipoConta.isPresent()) {
            try {
                tipoContaService.deleteById(id);
                return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
            } catch (final Exception e) {
                return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,
                                                                                       "Não é possível deletar um registro que esteja sendo utilizado em outro registro."))
                        .build();
            }
        } else {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Nao encontrado")).build();
        }
    }
}
