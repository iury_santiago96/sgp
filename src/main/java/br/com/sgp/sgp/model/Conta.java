package br.com.sgp.sgp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Conta extends BasicModel {

    private String nome;
    private BigDecimal valor;
    private Date dtConta;
    @ManyToOne
    @JoinColumn(name = "id_tipo_de_conta")
    private TipoConta tipoConta;
    @ManyToOne
    @JoinColumn(name = "id_voluntario")
    private Voluntario voluntario;

}
