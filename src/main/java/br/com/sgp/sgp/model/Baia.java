package br.com.sgp.sgp.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Baia extends BasicModel {

    private String nome;
    private Integer capacidade;
}
