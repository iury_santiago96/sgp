package br.com.sgp.sgp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Procedimento extends BasicModel {

    private String tipo;
    @Column(length = 9999999)
    private String descricao;
}
