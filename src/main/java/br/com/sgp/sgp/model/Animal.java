package br.com.sgp.sgp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Animal extends BasicModel {

    private String nome;
    private Date dtNascimento;
    private Date dtEntrada;
    private Boolean status;
    @ManyToOne
    @JoinColumn(name = "id_baia")
    private Baia baia;
    @ManyToOne
    @JoinColumn(name = "id_raca")
    private Raca raca;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_adocao")
    private Adocao adocao;

}
