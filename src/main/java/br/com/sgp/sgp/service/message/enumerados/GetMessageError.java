package br.com.sgp.sgp.service.message.enumerados;

import br.com.sgp.sgp.service.message.IMensagem;

public enum GetMessageError implements IMensagem {

    REGISTRO_ID_ERRO("Não é possível salvar um registro com um id já existente."),
    REGISTRO_DUPLICADO_ERRO("Já existe um registro igual à este salvo!"),
    REGISTRO_NAO_ENCONTRADO_EXCLUSAO("Náo foi possível encontrar o registro solicitado para exclusão!"),
    REGISTRO_NAO_ENCONTRADO_ALTERACAO("Náo foi possível encontrar o registro solicitado para alteração!"),
    REGISTRO_CPF_ERRO("Já existe um registro com esse CPF salvo no banco!"),
    REGISTRO_RG_ERRO("Já existe um registro com esse RG savo no banco!");
    String descricao;

    GetMessageError(final String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getTipo() {
        return "ERRO";
    }

    @Override
    public String getDescricao() {
        return descricao;
    }
}
